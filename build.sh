#!/usr/bin/env bash

ELECTRON_PROD=1

# Delete previous build
rm -R build

npm run build-assets
cp index.build.js build/index.js
cp package.json build/
npm run build