# Game Configurator for Shadow

An app to automatically configure your game and optimize them for Shadow.

## Installation

Download the artifacts from the gitlab and simply launch the file `GameConfigurator.exe`.

## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discordapp.com/invite/shadowtech)
- [Discord Shadow UK](https://discordapp.com/invite/ShadowEN)
- [Discord Shadow DE](https://discord.gg/shadowde)
- [Discord Shadow US](https://shdw.me/USDiscord)
- [Discord Shadow Community Projects](https://discord.gg/9HwHnHq)


## Contributing

```
npm i           # Install dependencies
npm run open    # Build and open the app (with watch and hot module reload)
npm run test    # Run unit tests (elm)
npm run e2e     # Run end to end tests (cypress) (requires that the project runs with npm run serve)
npm run cypress # Open cypress e2e tests
npm run serve   # Start a local server serving the html/js app (used for cypress, prefer npm run open)
npm run build   # Build the application for Windows
```

## Runner requirements

- Have the port 1234 available.
- Have ELM fully installed and functional.
- Have `unzip` installed.
- Have `wine` installed.


## Maintainers

![Nover#9563](https://cdn.discordapp.com/avatars/248726456551604224/4f22c1d6e37874987470c1af7dc21d10.png?size=64 "Nover#9563")

## Disclaimer

This is a community project, project is not affiliated to Blade in any way.

[Shadow](https://shadow.tech) logo and embeded Linux client is property of [Blade Group](http://www.blade-group.com/).
