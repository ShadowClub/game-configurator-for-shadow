module Main exposing (main)

import Browser exposing (Document)
import Element exposing (..)
import Element.Font exposing (..)
import Element.Region exposing (..)


main : Program Flags Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


type alias Flags =
    {}


type alias Model =
    {}


type Msg
    = MsgNoOp


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( {}
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    ( model
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


view : Model -> Document Msg
view model =
    { title = "Game Configurator for Shadow"
    , body =
        [ layout [] <|
            mainView model
        ]
    }


mainView : Model -> Element msg
mainView model =
    row [ height (px 20), width fill, centerX ]
        [ el [ heading 1, centerX, centerY ]
            (text "Game Configurator")
        ]
