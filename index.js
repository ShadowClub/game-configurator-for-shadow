const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const execa = require('execa');
const path = require('path');

app.isProduction = () => process.env.ELECTRON_PROD || false;

function runParcel() {
    return new Promise(resolve => {
        let output = '';
        const parcelProcess = execa('parcel', ['index.html']);
        const concat = (chunk) => {
            output += chunk;
            console.log(output);

            if (output.includes('Built in ')) {
                parcelProcess.stdout.removeListener('data', concat);
                console.log(output);
                resolve();
            }
        };
        parcelProcess.stdout.on('data', concat);
    });
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

async function createWindow() {
    mainWindow = new BrowserWindow({
        titleBarStyle: 'hidden',
        width: 1200,
        height: 800,
        icon: path.join(__dirname, 'src/images/icon.png'),
    });

    await runParcel();

    mainWindow.loadURL(`http://localhost:1234`);

    // Open the DevTools.
    // mainWindow.webContents.openDevTools()

    mainWindow.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
      createWindow();
  }
});
